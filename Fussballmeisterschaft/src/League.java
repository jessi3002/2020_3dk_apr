public class League {

    private int id;
    private String name;
    private String year;

    public League(int id, String name, String year) {
        this.id = id;
        this.name = name;
        this.year = year;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getYear() {
        return year;
    }

    @Override
    public String toString() {
        return "Championship{" +
                "id = " + id +
                ", name = " + name +
                ", year = " + year +
                "}";
    }

}
