import javax.swing.text.Style;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class FussballmeisterschaftMain {

    static ArrayList mannschaften = new ArrayList();
    static ArrayList mannschaften1 = new ArrayList();

    public static void main(String[] args) {

        FMdao fMdao = new FMdao("localhost", "3306", "fussballmeisterschaft", "root", "");
        Championship meisterschaft = fMdao.loadForLeague("1. Klasse Waldviertel");

        int count = 0;
        int t = 0;

        vereine(meisterschaft);

        while (t <= mannschaften.size()-1) {
            mannschaften1.add(Integer.toString(t));
            t += 1;
        }

        System.out.println("");
        System.out.println(mannschaften);

        for (int i = 1; i < mannschaften.size(); i++) {
            spieltage(meisterschaft, i);
            count=0;
            mannschaften1.set(mannschaften.size()-2, mannschaften.get(count));
            mannschaften1.set(count, mannschaften.get(count+1));
            count += 1;
            while (count <= mannschaften.size()-3) {
                mannschaften1.set(count, mannschaften.get(count+1));
                count += 1;
            }
            mannschaften1.set(mannschaften.size()-1, mannschaften.get(mannschaften.size()-1));
            mannschaften.clear();
            mannschaften.addAll(mannschaften1);
            System.out.println(mannschaften);
        }
        /*ArrayList games = new ArrayList();
        games = createTestgames(meisterschaft);
        GameDataAccessObject gameDataAccessObject = new GameDataAccessObject("localhost", "3306", "fussballmeisterschaft", "root", "");
        gameDataAccessObject.insert(games);




        /*
        System.out.println("");
        System.out.println("-----------------------------------------");

        List<String> teams = new ArrayList<>();
        teams.add("1");
        teams.add("2");
        teams.add("3");
        teams.add("4");
        teams.add("5");
        teams.add("6");

        String joker = teams.remove(teams.size()-1);

        for (int j = 0; j < teams.size(); j++) {
            System.out.println("Spieltag " + (j+1) + ":");

            int k = 0;

            while (k < teams.size()/2) {
                System.out.println(teams.get(k) + " : " + teams.get(teams.size()-1-k));
                k++;
            }
            System.out.println(teams.get(k) + " : " + joker);

            String te = teams.remove(0);
            teams.add(te);
        }*/

        System.out.println("");
        System.out.println("-----------------------------------------");

        ArrayList<Team> teams = meisterschaft.getTeam();

        ArrayList<Game> games = new ArrayList<>();

        Team joker = teams.remove(teams.size()-1);

        for (int j = 0; j < teams.size(); j++) {
            System.out.println("Spieltag " + (j+1) + ":");
            Date date = Date.valueOf(LocalDate.of(2021,1,j+1));

            int k = 0;

            while (k < teams.size()/2) {
                System.out.println(teams.get(k).getId() + " : " + teams.get(teams.size()-1-k).getId());
                games.add(new Game(1, teams.get(k).getId(), teams.get(teams.size()-1-k).getId(), 5, 2, date));
                k++;
            }
            System.out.println(teams.get(k).getId() + " : " + joker.getId());

            Team te = teams.remove(0);
            teams.add(te);
        }

        GameDataAccessObject gameDataAccessObject = new GameDataAccessObject("localhost", "3306", "fussballmeisterschaft", "root", "");
        gameDataAccessObject.insert(games);



    }

    public static void spieltage(Championship championship, int i) {
        int k = 0;

        System.out.println("Spieltag " + i + ":");

        System.out.print(mannschaften.get(k));
        System.out.print(" : ");
        System.out.println(mannschaften.get(mannschaften.size()-2));
        k += 1;

        while (mannschaften.size()/2 > 2 && k <= mannschaften.size()/2-2) {
            System.out.print(mannschaften.get(k));
            System.out.print(" : ");
            System.out.println(mannschaften.get(mannschaften.size()-(k+2)));
            k += 1;
        }

        System.out.print(mannschaften.get(mannschaften.size()/2-1));
        System.out.print(" : ");
        System.out.println(mannschaften.get(mannschaften.size()-1));
        System.out.println("");

    }

    public static void vereine(Championship championship) {
        String headline = championship.getLeague().getName() + " (" + championship.getLeague().getYear() + ")";
        System.out.println(headline);
        for (int i = 0; i < championship.getLeague().getName().length(); i++) {
            System.out.print("-");
        }
        System.out.println("");

        for (Team t : championship.getTeam()) {
            System.out.println(" " + t.getName());
            mannschaften.add(t.getName());
        }

    }

    /*public static ArrayList<Game> createTestgames(Championship cs) {
        ArrayList<Game> games = new ArrayList<Game>();
        games.add(new Game(1, cs.getTeam().get(1).getId(), cs.getTeam().get(2).getId(),2,4,Date.valueOf(LocalDate.of(2021,6,17))));
        games.add(new Game(2, cs.getTeam().get(3).getId(), cs.getTeam().get(4).getId(),4,3,Date.valueOf(LocalDate.of(2021,7,17))));
        games.add(new Game(3, cs.getTeam().get(5).getId(), cs.getTeam().get(6).getId(),1,5,Date.valueOf(LocalDate.of(2021,8,17))));
        return games;
    }*/
}
