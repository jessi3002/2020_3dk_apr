import java.util.Date;

public class Game {

    private int id;
    private int home_team;
    private int away_team;
    private int score_home;
    private int score_away;
    private Date datetime;

    public Game(int id, int home_team, int away_team, int score_home, int score_away, Date datetime) {
        this.id = id;
        this.home_team = home_team;
        this.away_team = away_team;
        this.score_home = score_home;
        this.score_away = score_away;
        this.datetime = datetime;
    }

    public int getId() {
        return id;
    }

    public int getHome_team() {
        return home_team;
    }

    public int getAway_team() {
        return away_team;
    }

    public int getScore_home() {
        return score_home;
    }

    public int getScore_away() {
        return score_away;
    }

    public Date getDatetime() {
        return datetime;
    }

    @Override
    public String toString() {
        return "Game{" +
                "id=" + id +
                ", home_team=" + home_team +
                ", away_team=" + away_team +
                ", score_home=" + score_home +
                ", score_away=" + score_away +
                ", datetime=" + datetime +
                '}';
    }
}
