import java.util.ArrayList;

public class Championship {

    private int id;
    private League league;
    private ArrayList<Team> team;

    public Championship(int id, League league) {
        this.id = id;
        this.league = league;
        this.team = new ArrayList<>();
    }

    public void addTeam(Team t) {
        this.team.add(t);
    }

    public int getId() {
        return id;
    }

    public League getLeague() {
        return league;
    }

    public ArrayList<Team> getTeam() {
        return team;
    }

    @Override
    public String toString() {
        return "Championship{" +
                "id = " + id +
                ", league = " + league +
                ", team = " + team +
                "}";
    }

}
