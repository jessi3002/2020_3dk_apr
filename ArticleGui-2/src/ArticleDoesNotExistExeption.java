public class ArticleDoesNotExistExeption extends RuntimeException{

    public ArticleDoesNotExistExeption(int articleId) {
        super("Article with id " + articleId + " does not exst");
    }
}
