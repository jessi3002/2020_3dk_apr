import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PanelB extends JPanel {

    public PanelB(ArticleTableModel tableModel, PanelA panelA, ArticleDataAccessObject articleDAO) {

        setLayout(new BorderLayout());

        JTable table = new JTable(tableModel);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        ListSelectionModel rowSelectionModel = table.getSelectionModel();
        rowSelectionModel.addListSelectionListener(new ArticleListSelectionListener(table, panelA));

        JScrollPane jScrollPane = new JScrollPane(table);
        jScrollPane.setPreferredSize(new Dimension(400, 120));
        add(jScrollPane, BorderLayout.NORTH);

        JButton showArticleListButton = new JButton("Zeige Artikelliste");
        showArticleListButton.setPreferredSize(new Dimension(100, 20));
        add(showArticleListButton, BorderLayout.WEST);
        showArticleListButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tableModel.initData(articleDAO.readAllArticles());
            }
        });

        JButton beendenButton = new JButton("Beenden");
        beendenButton.setPreferredSize(new Dimension(100, 20));
        add(beendenButton, BorderLayout.EAST);
        beendenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        setPreferredSize(new Dimension(400, 140));
    }
}
