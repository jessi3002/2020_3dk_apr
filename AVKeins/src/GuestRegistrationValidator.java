import java.lang.reflect.Array;
import java.util.ArrayList;

public class GuestRegistrationValidator{

    public void check(String txtvn, String txtnn, String txtte, String txtem, String txtti) throws GuestDataNotValidException{

        String errors = "";

        try {
            checkFirstname(txtvn);
        }catch (IllegalArgumentException e) {
            errors += e.getMessage() + "\n";
        }
        try {
            checkSurname(txtnn);
        }catch (IllegalArgumentException e) {
            errors += e.getMessage() + "\n";
        }
        try {
            checkNameError(txtvn, txtnn);
        }catch (IllegalArgumentException e) {
            errors += e.getMessage() + "\n";
        }
        try {
            checkMailAndNumber(txtte, txtem);
        }catch (IllegalArgumentException e) {
            errors += e.getMessage() + "\n";
        }
        try {
            checkTablenumber(txtti);
        }catch (IllegalArgumentException e) {
            errors += e.getMessage() + "\n";
        }
        if (!errors.isEmpty()) {
            throw new GuestDataNotValidException(errors.toString());
        }
    }

    private void checkFirstname(String txtvn) {
        if (checkEmpty(txtvn) == true || checkLength(txtvn, 15)) {
            throw new IllegalArgumentException("First name must not be empty or longer than 15 characters.");
        }
    }
    private void checkSurname(String txtnn) {
        if (checkEmpty(txtnn) == true || checkLength(txtnn, 20)){
            throw new IllegalArgumentException("Last name must not be empty or longer than 20 characters.");
        }
    }
    private void checkNameError(String txtvn, String txtnn) {
        if (txtvn.equalsIgnoreCase("Max") && txtnn.equalsIgnoreCase("Mustermann")) {
            throw new IllegalArgumentException("Max Mustermann is not allowed!");
        }
    }
    private void checkMailAndNumber(String txtte, String txtem) {
        if (checkEmpty(txtte) == true && checkEmpty(txtem) == true) {
            throw new IllegalArgumentException("Email or phone number must be provided.");
        }
    }
    private void checkTablenumber(String txtti) {
        if (txtti.isEmpty()) {
            throw new IllegalArgumentException("Table number must be between 1 and 100.");
        }
        if (Integer.parseInt(txtti) < 1 || Integer.parseInt(txtti) > 100) {
                throw new IllegalArgumentException("Table number must be between 1 and 100.");
        }
    }

    private boolean checkEmpty(String value) {
        if (value.isEmpty()) {
            return true;
        }else{
            return false;
        }
    }
    private boolean checkLength(String value, Integer number) {
        if (value.length() > number) {
            return true;
        }else{
            return false;
        }
    }

}
