public class GuestDataNotValidException extends Exception{

    private String message;

    public GuestDataNotValidException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
