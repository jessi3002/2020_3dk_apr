import javax.swing.*;
import java.awt.*;

public class Canvas extends JPanel { //immer JPanel

    private FunctionPlutterProgram functionPlutter;

    public Canvas(FunctionPlutterProgram functionPlutter) {
        setBackground(Color.BLACK);
        setForeground(Color.RED);
        setPreferredSize(new Dimension(500, 500));

    }

    @Override
    protected void paintComponent(Graphics g) { //alt+eif.
        super.paintComponent(g);

        g.translate(250,250);

        g.drawLine(0, -220, 0, 250);
        g.drawLine(-250, 0, 250, 0);

        g.drawLine(-10,-20,10,-20);
        g.drawLine(-10,20,10,20);
        g.drawString("-1", -15,-25);
        g.drawString("+1", 10,30);

        g.drawLine(-20,-10,-20,10);
        g.drawLine(20,-10,20,10);
        g.drawString("-1", 30,-10);
        g.drawString("+1", -30,-10);

        double y = 0;

        g.setColor(Color.YELLOW);
        for (double i = - 10; i < 10; i+= 0.005) {
            if (functionPlutter.getFunction() == 1) {
                y = Math.tan(i);
            }else {
                y = Math.pow(i,2);
            }
            y = Math.tan(i);
            int xPos = (int) (i * 50);
            int yPos = (int) (-y * 50);
            g.drawOval(xPos, yPos,1,1);
        }

    }
}