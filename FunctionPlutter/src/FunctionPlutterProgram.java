import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FunctionPlutterProgram extends JFrame {

    private Canvas drawArea;

    private int function;

    public int getFunction() {
        return function;
    }

    public FunctionPlutterProgram(String title){

        super(title);
        setLayout(new FlowLayout());

        Canvas drawArea = new Canvas(this);
        add(drawArea);
        drawArea.setPreferredSize(new Dimension(500,500));
        add(drawArea);

        JPanel pnlButton = new JPanel();

        pnlButton.setLayout(new GridLayout(2,1,20,20));
        JButton btnF1 = new JButton("tan(x)");
        btnF1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                function = 1;
                drawArea.repaint();
            }
        });

        JButton btnF2 = new JButton("x2");
        btnF2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                function = 2;
                drawArea.repaint();
            }
        });
        pnlButton.add(btnF2);
        pnlButton.add(btnF1);

        JButton tamButton = new JButton("tam(x)");
        add(tamButton);

        JButton powerButton = new JButton("x^2");
        add(powerButton);

        setVisible(true);
        setSize(700,550);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);




    }


}


