import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseAccess {

    private String user;

    private String password;

    private String connectionurl;

    public DatabaseAccess(String host, String port, String dbName, String user, String password, String dbVendor)throws DbVendorNotSupportedException {
        this.user = user;
        this.password = password;
        if ("mariadb".equals(dbVendor)) {
            initMariaDb(host, port, dbName);
        } else if ("oracle".equals(dbVendor)) {
            initOracleDb(host, port, dbName);
        }
        //throw new RuntimeException(dbVendor + " is currently not support. use oracle or mariadb!");
        throw new DbVendorNotSupportedException(dbVendor + " ist not supported!");
    }

    public void initMariaDb (String host, String port, String dbName) {
        this.connectionurl = "jdbc:mariadb://" + host + ":" + port + "/" + dbName;
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not load database driver", e);
        }
    }

    public void initOracleDb (String host, String port, String dbName) {
        this.connectionurl = "jdbc:oracle:thin:@" + host + ":" + port + ":" + dbName;
        try {
            Class.forName("oracle.jdbc.OracleDriver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not load database driver", e);
        }
    }

    public Connection connect() {
        System.out.println("Connecting to database");
        try {
            return DriverManager.getConnection(this.connectionurl, this.user, this.password);
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not connect to database", throwables);
        }
    }

    public void close(Connection connection) {
        System.out.println("Closing connection");
        try {
            connection.close();
        } catch (SQLException throwables) {
            System.err.println("Could not close connection: " + throwables.getMessage());
        }
    }

}
