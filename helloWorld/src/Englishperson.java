public class Englishperson extends Person {

    public Englishperson (String name) {
        super(name);
    }

    @Override
    public String greet(){
        return "Hello " + this.name;
    }

}
