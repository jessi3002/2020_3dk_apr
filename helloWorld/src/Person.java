public abstract class Person {
    protected String name;

    public Person(String name) {
        if (name.equals("Johann")) {
            this.name = "Hansi";
        } else {
            this.name = name;
        }
    }

    public abstract String greet();
}
