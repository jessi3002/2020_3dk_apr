public class GermanPerson extends Person{


    public GermanPerson(String name) {
        super(name);
        }

    public String greet(){
        return "Hallo " + this.name;
    }
}
