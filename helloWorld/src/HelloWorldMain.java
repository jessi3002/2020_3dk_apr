import javax.imageio.stream.FileImageInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

public class HelloWorldMain {

    public static void main(String[] args) throws IOException {

        /*System.out.println("Read specific environment variable");
        System.out.println("KONFIG: " + System.getenv("KONFIG"));

        Map<String, String> env = System.getenv();
        for (Map.Entry<String, String> entry: env.entrySet()) {
            System.out.println("Variable Name: " + entry.getKey() + ", Value: " + entry.getValue());*/

        handleProgramArguments(args);
        handlePropertiesFile();
        handleProperties();
        handleEnvironmentVariables();

        }


        /*
        "JAVA_HOME"= "....",
        ....
         */

        /*Properties properties = new Properties();
        FileInputStream in = new FileInputStream("config.properties");
        properties.load(in);
        in.close();
        System.out.println("host=" + properties.getProperty("host"));
        System.out.println("port=" + properties.getProperty("port"));

        /*String host = System.getProperty("host");
        System.out.println("host=" + host);
        String port = System.getProperty("port");
        System.out.println("port=" + port);



        /*
        Englishperson englishPerson = new Englishperson("Johann");



        SpanishPerson spanishPerson = new SpanishPerson("Johann");


        GermanPerson germanPerson = new GermanPerson("Johann");

        Frenchperson frenchPerson= new Frenchperson("Johann");



        ArrayList<Person> persons = new ArrayList<>();
        persons.add(englishPerson);
        persons.add(spanishPerson);
        persons.add(germanPerson);
        persons.add(frenchPerson);

        for (Person p : persons) {
            System.out.println(p.greet());
        }*/


    private static void handleProgramArguments(String[] args) {
        System.out.println("Hello");
        for (String s: args) {
            System.out.println(s);
        }

        if (args.length == 0) {
            System.err.println("Mindestens 1 argument muss übergeben werden"); //irgendwo in product structure //java.docx nachschaun
            System.exit(-1);
        }
        System.out.println(args[0]);
    }

    private static void handlePropertiesFile() throws IOException {
        Properties properties = new Properties();
        FileInputStream in = new FileInputStream("config.properties");
        properties.load(in);
        in.close();
        System.out.println("host=" + properties.getProperty("host"));
        System.out.println("port=" + properties.getProperty("port"));
    }

    private static void handleProperties() throws IOException {
        String host = System.getProperty("host"); //irgendwo in product structure //java.docx nachschaun
        System.out.println("host=" + host);
        String port = System.getProperty("port");
        System.out.println("port=" + port);
    }

    private static void handleEnvironmentVariables() {
        System.out.println("Read specific environment variable");
        System.out.println("KONFIG: " + System.getenv("KONFIG"));

        Map<String, String> env = System.getenv();
        for (Map.Entry<String, String> entry : env.entrySet()) {
            System.out.println("Variable Name: " + entry.getKey() + ", Value: " + entry.getValue());
        }
    }
}