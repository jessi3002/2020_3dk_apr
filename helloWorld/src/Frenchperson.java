public class Frenchperson extends Person{
    public Frenchperson(String name) {
        super(name);
    }

    @Override
    public String greet() {
        return "Bon jour "+ name;
    }
}
