import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EnglishpersonTest {

    @Test
    void greet() {
        Englishperson greeter = new Englishperson("Tim");
        assertEquals("Hello Tim", greeter.greet());
    }

    @Test
    void greetWithNickname(){
        Englishperson greeter = new Englishperson("Johann");
        assertEquals("Hello Hansi", greeter.greet());
    }
}