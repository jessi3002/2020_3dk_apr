import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class TimeGUI extends JFrame {

    private JLabel timelabel;
    private Color[] colors = {Color.BLACK, Color.BLUE, Color.RED, Color.GRAY};
    private int currentColorPosition;

    public TimeGUI() {
        setTitle("Uhrzeit anzeigen");
        setLayout(new FlowLayout());

        JPanel contentpane = new JPanel();
        setContentPane(contentpane);

        this.timelabel = new JLabel(getTimeText());

        Timer timer = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setTime();
            }
        });
        timer.start();

        JButton changeColorButton = new JButton("Farbwechsel starten");
        changeColorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Timer t = new Timer(3000, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        currentColorPosition++;
                        if (currentColorPosition > colors.length - 1) {
                            currentColorPosition = 0;
                        }
                        timelabel.setForeground(colors[currentColorPosition]);
                    }
                });
                t.start();
                changeColorButton.setEnabled(false);
            }
        });

        contentpane.add(changeColorButton);
        contentpane.add(timelabel);


        setSize(200,200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        pack();

    }

    public void setTime() {
        timelabel.setText(getTimeText());
    }

    private String getTimeText() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        return LocalTime.now().format(dateTimeFormatter);
    }

}
