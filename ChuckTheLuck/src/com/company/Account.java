package com.company;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class Account extends JPanel {
    private JLabel lblScore;
    private Game game;
    private JButton btnAddStake;

    public JLabel getLblScore() {
        return this.lblScore;
    }

    public void setLblScore(String lblScore) {
        this.lblScore.setText(lblScore);
    }

    public JButton getBtnAddStake() {
        return this.btnAddStake;
    }

    public void setBtnAddStake(JButton btnAddStake) {
        this.btnAddStake = btnAddStake;
    }

    public Account(Game game) {
        this.game = game;
        this.setLayout(new FlowLayout(1, 10, 20));
        this.setBorder(new EmptyBorder(6, 10, 10, 10));
        this.setPreferredSize(new Dimension(160, 0));
        this.setBackground(Color.LIGHT_GRAY);
        JLabel lblAccount = new JLabel("Konto");
        lblAccount.setHorizontalAlignment(0);
        lblAccount.setPreferredSize(new Dimension(120, 30));
        lblAccount.setOpaque(true);
        lblAccount.setBackground(Color.WHITE);
        this.add(lblAccount);
        this.lblScore = new JLabel();
        this.lblScore.setText("0");
        this.lblScore.setHorizontalAlignment(0);
        this.lblScore.setPreferredSize(new Dimension(30, 30));
        this.lblScore.setOpaque(true);
        this.lblScore.setBackground(Color.WHITE);
        this.add(this.lblScore);
        this.btnAddStake = new JButton("Einsatz zahlen");
        this.btnAddStake.setPreferredSize(new Dimension(120, 30));
        this.add(this.btnAddStake);
        this.btnAddStake.addActionListener(game);
    }
}
