package com.company;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Interface extends JFrame {
    private final String[] ARR = new String[]{"lblDice1", "lblDice2", "lblDice3"};
    private JPanel heading = new JPanel();
    private JLabel lblHeading = new JLabel("CHUCK A LUCK");
    private Game myGame = new Game(this);
    private Account myAccount;
    private SetNumber myNumber;
    private ThrowDice myDice;

    public Game getMyGame() {
        return this.myGame;
    }

    public void setMyGame(Game myGame) {
        this.myGame = myGame;
    }

    public ThrowDice getMyDice() {
        return this.myDice;
    }

    public void setMyDice(ThrowDice myDice) {
        this.myDice = myDice;
    }

    public SetNumber getMyNumber() {
        return this.myNumber;
    }
    
    public Account getMyAccount() {
        return this.myAccount;
    }

    public void setMyAccount(Account myAccount) {
        this.myAccount = myAccount;
    }

    public Interface(String title) {
        super(title);
        this.myAccount = new Account(this.myGame);
        this.myNumber = new SetNumber();
        this.myDice = new ThrowDice(this.myGame);
        this.add(this.myAccount, "West");
        this.add(this.myNumber, "Center");
        this.add(this.myDice, "East");
        this.heading.setBackground(Color.BLUE);
        this.heading.add(this.lblHeading);
        this.lblHeading.setForeground(Color.WHITE);
        this.lblHeading.setFont(new Font("Times New Roman", 1, 18));
        this.lblHeading.setHorizontalAlignment(0);
        this.add(this.heading, "North");
    }
}
