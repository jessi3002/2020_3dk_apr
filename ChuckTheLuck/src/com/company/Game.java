package com.company;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class Game implements ActionListener {
    private Interface anInterface;
    Random rnd = new Random();
    private int counter = 0;

    public Game(Interface anInterface) {
        this.anInterface = anInterface;
    }

    public void actionPerformed(ActionEvent e) {
        // hier wären zwei ActionListener Klassen besser gewesen, damit könnte man das "Würfeln" und das "Einsatz zahlen" aufteilen
        // was den Code übersichtlicher macht
        if (e.getSource().equals(this.anInterface.getMyAccount().getBtnAddStake())) {
            this.anInterface.getMyAccount().setLblScore("5");
            this.anInterface.getMyAccount().getBtnAddStake().setEnabled(false);
            this.anInterface.getMyDice().getBtnDice().setEnabled(true);
        }

        if (e.getSource().equals(this.anInterface.getMyDice().getBtnDice())) {
            this.anInterface.getMyDice().setLblDice1(Integer.toString(this.rnd.nextInt(6) + 1));
            this.anInterface.getMyDice().setLblDice2(Integer.toString(this.rnd.nextInt(6) + 1));
            this.anInterface.getMyDice().setLblDice3(Integer.toString(this.rnd.nextInt(6) + 1));
            this.counter = 0;
            if (this.anInterface.getMyDice().getLblDice1().getText().equals(this.anInterface.getMyNumber().getTxtSetNumber().getText())) {
                ++this.counter;
            }

            if (this.anInterface.getMyDice().getLblDice2().getText().equals(this.anInterface.getMyNumber().getTxtSetNumber().getText())) {
                ++this.counter;
            }

            if (this.anInterface.getMyDice().getLblDice3().getText().equals(this.anInterface.getMyNumber().getTxtSetNumber().getText())) {
                ++this.counter;
            }

            if (this.counter > 0) {
                this.anInterface.getMyAccount().setLblScore(String.valueOf(Integer.parseInt(this.anInterface.getMyAccount().getLblScore().getText()) + this.counter));
            } else {
                this.anInterface.getMyAccount().setLblScore(String.valueOf(Integer.parseInt(this.anInterface.getMyAccount().getLblScore().getText()) - 1));
                if (this.anInterface.getMyAccount().getLblScore().getText().equals("0")) {
                    this.anInterface.getMyAccount().getBtnAddStake().setEnabled(true);
                    this.anInterface.getMyDice().getBtnDice().setEnabled(false);
                }
            }
        }

    }
}
