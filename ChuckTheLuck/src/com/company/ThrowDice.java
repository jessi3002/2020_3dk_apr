package com.company;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ThrowDice extends JPanel
{
    private JButton btnDice;
    private JLabel lblDice1;
    private JLabel lblDice2;
    private JLabel lblDice3;
    private com.company.Game game;

    public JButton getBtnDice() {
        return btnDice;
    }

    public void setBtnDice(JButton btnDice) {
        this.btnDice = btnDice;
    }

    public void setLblDice1(String lblDice1) {
        this.lblDice1.setText(lblDice1);
    }

    public void setLblDice2(String lblDice2) {
        this.lblDice2.setText(lblDice2);
    }

    public void setLblDice3(String lblDice3) {
        this.lblDice3.setText(lblDice3);
    }

    public JLabel getLblDice1() {
        return lblDice1;
    }

    public JLabel getLblDice2() {
        return lblDice2;
    }

    public JLabel getLblDice3() {
        return lblDice3;
    }

    public ThrowDice(com.company.Game game)
    {
        this.game = game;
        setLayout(new FlowLayout(FlowLayout.CENTER,10,10));

        setBorder(new EmptyBorder(16,10,15,10));
        setPreferredSize(new Dimension(160, 0));
        setBackground(Color.LIGHT_GRAY);

        JLabel lblDice = new JLabel("Würfeln");
        lblDice.setPreferredSize(new Dimension(120,30));
        lblDice.setHorizontalAlignment(SwingConstants.CENTER);
        lblDice.setOpaque(true);
        lblDice.setBackground(Color.WHITE);
        add(lblDice);

        JPanel dicePanel = new JPanel();
        dicePanel.setLayout(new FlowLayout(FlowLayout.CENTER,10,10));
        dicePanel.setOpaque(false);
        lblDice1 = new JLabel("");
        lblDice1.setPreferredSize(new Dimension(30,30));
        lblDice1.setHorizontalAlignment(SwingConstants.CENTER);
        lblDice1.setOpaque(true);
        lblDice1.setBackground(Color.WHITE);
        dicePanel.add(lblDice1);

        lblDice2 = new JLabel("");
        lblDice2.setPreferredSize(new Dimension(30,30));
        lblDice2.setHorizontalAlignment(SwingConstants.CENTER);
        lblDice2.setOpaque(true);
        lblDice2.setBackground(Color.WHITE);
        dicePanel.add(lblDice2);

        lblDice3 = new JLabel("");
        lblDice3.setPreferredSize(new Dimension(30,30));
        lblDice3.setHorizontalAlignment(SwingConstants.CENTER);
        lblDice3.setOpaque(true);
        lblDice3.setBackground(Color.WHITE);
        dicePanel.add(lblDice3);

        add(dicePanel);

        btnDice = new JButton("Würfeln");
        btnDice.setPreferredSize(new Dimension(120,30));
        add(btnDice);
        btnDice.setEnabled(false);
        btnDice.addActionListener(game);
    }
}

