import javax.swing.*;
import java.awt.*;

public class GuestRegisterData extends JPanel {

    public GuestRegisterData() {

        setLayout(null); // hier wäre LayoutManagement sinnvoll -1

        JLabel guestregister = new JLabel("Gästeregistierung");
        guestregister.setFont(new Font("Arial",Font.BOLD,50));
        guestregister.setLocation(350,10); // hier ist setBounds(...) zu verwenden -1
        this.add(guestregister);

        //Panel panel = new Panel();
        //add(panel);
        //panel.setLayout(new GridLayout(5,2));

        JLabel name = new JLabel("Vorname: ");
        name.setBounds(20,40,100,100);
        this.add(name);

        JTextField TFname = new JTextField();
        name.setBounds(70,40,300,100);
        this.add(TFname);

        JLabel telefone = new JLabel("Telefonnummer: ");
        telefone.setBounds(170,40,300,100);
        this.add(telefone);

        JTextField TFtelefone = new JTextField();
        name.setBounds(250,40,300,100);
        this.add(TFtelefone);




        JLabel surName = new JLabel("Nachname: ");
        surName.setBounds(20,70,300,100);
        this.add(surName);

        JTextField TFsurName = new JTextField();
        TFsurName.setBounds(70,70,300,100);
        this.add(TFsurName);

        JLabel eMail = new JLabel("E-Mail: ");
        eMail.setBounds(170,70,300,100);
        this.add(eMail);

        JTextField TFeMail = new JTextField();
        TFeMail.setBounds(250,70,300,100);
        this.add(TFeMail);



        JLabel street = new JLabel("Nachname: "); // falsche Labelbezeichnung: -0,5
        street.setBounds(20,100,300,100);
        this.add(street);

        JTextField TFstreet = new JTextField();
        TFstreet.setBounds(70,100,300,100);
        this.add(TFstreet);

        JLabel table = new JLabel("E-Mail: "); // falsche Labelbezeichnung: -0,5
        table.setBounds(170,100,300,100);
        this.add(table);

        JTextField TFtable = new JTextField();
        TFtable.setBounds(250,100,300,100);
        this.add(TFtable);

        JLabel place = new JLabel("Nachname: "); // falsche Labelbezeichnung: -0,5
        place.setBounds(20,130,300,100);
        this.add(place);

        // PLZ fehlt: -1
        // Ort fehlt: -1


        Panel panel = new Panel();
        this.add(panel);
        panel.setLayout(new FlowLayout());
        panel.setVisible(true);

        JButton save = new JButton("Speichern");
        save.setPreferredSize(new Dimension(300,100));
        this.add(save); // der save-Button und der setback Button sollten auf das panel -1

        JButton setback = new JButton("Zurücksetzen");
        setback.setPreferredSize(new Dimension(300,100));
        this.add(setback);



    }


}
