import javax.swing.*;
import java.awt.*;

public class GuestRegisterGUI extends JFrame {

    public GuestRegisterGUI() {
        super("COVID-19 Gastregistrierung");


        add(new GuestRegisterData());


        setVisible(true);
        setSize(700,700);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        pack();
    }
}
