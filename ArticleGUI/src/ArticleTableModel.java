import javax.swing.table.AbstractTableModel;
import java.util.List;

public class ArticleTableModel extends AbstractTableModel {

    private final String[] columName = {"id", "Description", "Price", "Stock"};

    private List<Article> articles;

    public void iniData(List<Article> articles) {
        this.articles = articles;
        fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        if (articles != null) {
            return articles.size();
        }
        return 0;
    }

    @Override
    public int getColumnCount() {
        return columName.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Article article = articles.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return article.getArticleNumber();
            case 1:
                return article.getArticleName();
            case 2:
                return article.getArticlePrice();
            case 3:
                return article.getArticleStock();
            default:
                return "";
        }
    }

    @Override
    public String getColumnName(int column) {
        return columName[column];
    }
}
