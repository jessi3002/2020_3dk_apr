import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ArticleDataAccessObject {

    private final String user;
    private final String password;
    private final String databaseurl;
    public HashMap<Integer, Article> database;

    public ArticleDataAccessObject(String host, String port, String database, String user, String password) {
        this.user = user;
        this.password = password;
        this.database = new HashMap<>();
        this.databaseurl = "jdbc:mariadb://" + host + ":" + port + "/" + database;
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not load database driver", e);
        }
    }

    public Article read(int id) {
        Connection connection = connect();
        String sql = "select * from article where id = " + id;
        Statement select = null;
        try {
            select = connection.createStatement();
            ResultSet resultSet = select.executeQuery(sql);
            if (resultSet.next()) {
                return new Article(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getDouble(3),
                        resultSet.getInt(4));
            }
        } catch (SQLException throwables) {
            System.err.println("Could not run query!");
            throwables.printStackTrace();
        } finally {
            close(connection);
        }
        return null;
    }

    public List<Article> readAllMassages() {
        Connection connection = connect();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            String sql = "select * from article";
            ResultSet resultSet = statement.executeQuery(sql);
            ArrayList<Article> article = new ArrayList<>();
            while (resultSet.next()) {
                int id = resultSet.getInt(1);
                String description = resultSet.getString(2);
                int price = resultSet.getInt(3);
                int stock = resultSet.getInt(4);
                article.add(new Article(id, description, price, stock));
            }
            close(connection);
            return article;
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not create statement", throwables);
        } finally {
            close(connection);
        }
    }

    public void insert(Article article) throws ArticleAlreadyExistsException {
        if (database.containsKey(article.getArticleNumber())) {
            throw new ArticleAlreadyExistsException(article.getArticleNumber());
        }
        this.database.put(article.getArticleNumber(), article);
    }

    public void delete(int articleId) {
        if (database.containsKey(articleId)) {
            this.database.remove(articleId);
        }else {
            throw new ArticleDoesNotExitException(articleId);
        }
    }

    @Override
    public String toString() {
        return "ArticleDatabase{" +
                "database=" + database +
                '}';
    }

    private Connection connect() {
        try {
            return DriverManager.getConnection(this.databaseurl, this.user, this.password);
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not connect to database", throwables);
        }
    }

    private void close(Connection connection) {
        try {
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
