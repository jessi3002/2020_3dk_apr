public class ArticleAlreadyExistsException extends Exception{

    public ArticleAlreadyExistsException(int articleID) {
        super("Article with id " + articleID + " already exists");
    }

}
