public class ArticleDoesNotExitException extends RuntimeException{

    public ArticleDoesNotExitException(int articleId) {
        super("Article with the " + articleId + " does not exist");
    }
}
