import javax.swing.*;
import java.awt.*;

public class ArticleListPanel extends JPanel {

    private JPanel buttonPanel;
    private JPanel centerPanel;

    private JTextArea textAreaMain;
    private JScrollPane scrollPane;

    private JButton buttonShowArticles;
    private JButton buttonExit;

    private JLabel placeholderLeft;
    private JLabel placeholderRight;
    private JLabel placeholderBottom;

    public ArticleListPanel (ArticleDataAccessObject articleDao, ArticleTableModel tableModel) {
        setLayout(new BorderLayout());
        initComps(articleDao, tableModel);

        add(centerPanel, BorderLayout.CENTER);
        add(placeholderLeft, BorderLayout.WEST);
        add(placeholderRight, BorderLayout.EAST);
        add(placeholderBottom, BorderLayout.SOUTH);
    }

    private void initComps(ArticleDataAccessObject articleDao, ArticleTableModel tableModel) { //PanelB
        buttonPanel = new JPanel();
        centerPanel = new JPanel();


        BorderLayout centerPanelBorderLayout = new BorderLayout();
        centerPanelBorderLayout.setVgap(15);
        centerPanel.setLayout(centerPanelBorderLayout);


        JTable table = new JTable(tableModel);


        placeholderLeft = new JLabel("            ");
        placeholderRight = new JLabel("            ");
        placeholderBottom = new JLabel("            ");


        scrollPane = new JScrollPane(table);
        scrollPane.setPreferredSize(new Dimension(200,200));
        centerPanel.add(scrollPane);

        buttonShowArticles = new JButton("Zeige Artikelliste");
        buttonExit = new JButton("Beenden");

        buttonPanel.setLayout(new BorderLayout());
        buttonPanel.add(buttonShowArticles,BorderLayout.WEST);
        buttonPanel.add(buttonExit,BorderLayout.EAST);

        centerPanel.add(buttonPanel, BorderLayout.SOUTH);

    }
}
