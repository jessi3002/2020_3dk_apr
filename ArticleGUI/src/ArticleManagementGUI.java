import javax.swing.*;
import java.awt.*;

public class ArticleManagementGUI extends JFrame {

    private JPanel contentPane;

    public ArticleManagementGUI(){
        contentPane = new JPanel();
        setContentPane(contentPane);

        ArticleDataAccessObject articleDAO = new ArticleDataAccessObject("localhost", "3306", "article_management", "root", "");

        ArticleTableModel tableModel = new ArticleTableModel();
        tableModel.iniData(articleDAO.readAllMassages());

        contentPane.add(new ArticleListPanel(articleDAO, tableModel));
        contentPane.add(new ProductInputPanel(articleDAO, tableModel));

        contentPane.setLayout(new BorderLayout());
        contentPane.add(new ProductInputPanel(articleDAO, tableModel), BorderLayout.CENTER);
        contentPane.add(new ArticleListPanel(articleDAO, tableModel),BorderLayout.SOUTH);

        setSize(500,475);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setVisible(true);
    }
}
