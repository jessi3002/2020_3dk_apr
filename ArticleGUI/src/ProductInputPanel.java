import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class ProductInputPanel extends JPanel {

    private JLabel labelTitle;
    private JLabel labelArticleNumber;
    private JLabel labelArticleName;
    private JLabel labelPrice;
    private JLabel labelStorageAmount;

    private JButton buttonNew;
    private JButton buttonCreate;
    private JButton buttonChange;
    private JButton buttonDelete;
    private JButton buttonSearch;

    private JTextField fieldArticleNumber;
    private JTextField fieldArticleName;
    private JTextField fieldPrice;
    private JTextField fieldStorageAmount;
    private JTextField filedArticleDesc;

    ArticleDataAccessObject articleDAO = new ArticleDataAccessObject("localhost", "3306", "article_management", "root", "");


    public ProductInputPanel (ArticleDataAccessObject articleDao, ArticleTableModel tableModel) {

        setLayout(null);

        initComponents(articleDao, tableModel);
        setCompsBounds();
    }

    public ArticleDataAccessObject getArticleDao(ArticleDataAccessObject articleDao) {
        return articleDao;
    }

    public ArticleTableModel getTableModel(ArticleTableModel tableModel) {
        return tableModel;
    }

    public void loadArticle(int id) {
        Article article = articleDAO.read(id);
        fieldArticleNumber.setText(article.getArticleNumber() + "");
        filedArticleDesc.setText(article.getArticleName());
        fieldPrice.setText(article.getArticlePrice() + "");
        fieldStorageAmount.setText(article.getArticleStock() + "");
    }

    public void initComponents (ArticleDataAccessObject articleDao, ArticleTableModel tableModel) {
        labelTitle = new JLabel("Artikel");
        labelTitle.setFont(new Font("Arial", Font.BOLD,20));
        labelArticleNumber = new JLabel("Artikelnummer:");
        labelArticleName = new JLabel("Artikelbezeichnung:");
        labelPrice = new JLabel("Verkaufspreis:");
        labelStorageAmount = new JLabel("Bestand:");

        ArticleDatabase database = null;
        try {
            database = new ArticleDatabase();
        } catch (ArticleAlreadyExistsException articleAlreadyExistsException) {
            articleAlreadyExistsException.printStackTrace();
        }

        fieldArticleNumber = new JTextField();
        fieldArticleName = new JTextField();
        fieldPrice = new JTextField();
        fieldStorageAmount = new JTextField();

        buttonNew = new JButton("Neu");
        buttonCreate = new JButton("Anlegen");



        buttonChange = new JButton("Ändern");
        buttonDelete = new JButton("Löschen");
        buttonDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ArticleDataAccessObject articleDAO = new ArticleDataAccessObject("Localhost", "3306", "hello_world", "root", "");
                int articleId = checkInteger(fieldArticleNumber.getText());
                //database.delete(articleId);
                articleDAO.delete(articleId);
                System.out.println(articleDAO.database);
            }
        });
        buttonSearch = new JButton("Suchen");

        buttonCreate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    newArticle(tableModel);
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        });

        JTable table = new JTable(tableModel);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        ListSelectionModel rowSelectionModel = table.getSelectionModel();
        ProductInputPanel productInputPanel = new ProductInputPanel(articleDao, tableModel);
        rowSelectionModel.addListSelectionListener(new ArticleSelectionListener(table, productInputPanel));

    }

    private void setCompsBounds () {
        add(labelTitle);
        labelTitle.setBounds(10,10,150,25);
        add(buttonNew);
        buttonNew.setBounds(350,10,125,25);

        add(labelArticleNumber);
        labelArticleNumber.setBounds(10,60,150,25);
        add(fieldArticleNumber);
        fieldArticleNumber.setBounds(165,60,150,25);
        add(buttonCreate);
        buttonCreate.setBounds(350,60,125,25);

        add(labelArticleName);
        labelArticleName.setBounds(10,90,150,25);
        add(fieldArticleName);
        fieldArticleName.setBounds(165,90,150,25);
        add(buttonChange);
        buttonChange.setBounds(350,90,125,25);

        add(labelPrice);
        labelPrice.setBounds(10,120,150,25);
        add(fieldPrice);
        fieldPrice.setBounds(165,120,150,25);
        add(buttonDelete);
        buttonDelete.setBounds(350,120,125,25);

        add(labelStorageAmount);
        labelStorageAmount.setBounds(10,150,150,25);
        add(fieldStorageAmount);
        fieldStorageAmount.setBounds(165,150,150,25);
        add(buttonSearch);
        buttonSearch.setBounds(350,150,125,25);
    }

    public String getArticleId() {
        return checkEmpty(fieldArticleNumber.getText()); }

    public String getArticleName () {
        return checkEmpty(fieldArticleName.getText());
    }

    public String getArticlePrice () {
        return checkEmpty(fieldPrice.getText());
    }

    public String getArticleStock (){
        return checkEmpty(fieldStorageAmount.getText());
    }

    private String checkEmpty (String value) {
        if (value == null || value.isEmpty()) {
            throw new IllegalArgumentException("Eingabe darf nicht leer sein");
        }
        return value;
    }

    private String checkMaxLength(String value, int maxLength) throws IllegalArgumentException {
        if (value.length() > maxLength) {
            throw new IllegalArgumentException("Eingabe darf nicht länger als " + maxLength + " sein.");
        }
        return value;
    }

    private int checkInteger(String value) throws IllegalArgumentException {
        try {
            return Integer.parseInt(value);
        }catch (NumberFormatException e) {
            throw new IllegalArgumentException("Eingabe muss eine ganzzahlige Zahl sein");
        }
    }

    private double checkDouble(String value) throws IllegalArgumentException {
        try {
            return Double.parseDouble(value);
        }catch (NumberFormatException e) {
            throw new IllegalArgumentException("Eingabe muss eine Zahl sein");
        }
    }

    public void newArticle(ArticleTableModel tableModel) throws SQLException{

        Connection connection = null;

        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/article_management", "root", "");
        System.out.println("Connected to database successfully");

        Article art = new Article(Integer.parseInt(getArticleId()), getArticleName(),Double.parseDouble(getArticlePrice()),Integer.parseInt(getArticleStock()));

        String sql = "insert into article(id, description, price, stock) VALUES ('" + art.getArticleNumber() + "','" + art.getArticleName() + "','" + art.getArticlePrice() + "','" + art.getArticleStock() + "')";
        Statement insert = connection.createStatement();
        boolean success = insert.execute(sql);
        //List<Article> articles = getArticleDao().readAllMassages();
        List<Article> articles = getArticleDao().readAllMassages();
        System.out.println("Insert executed successfully");
        tableModel.iniData(articles);
        //hier irgendwo fehler

    }
}
