public class Article {
    private final int articleNumber;
    private final String articleName;
    private final double articlePrice;
    private final int articleStock;


    public Article(int articleNumber, String articleName, double articlePrice, int articleStock) {
        this.articleNumber = articleNumber;
        this.articleName = articleName;
        this.articlePrice = articlePrice;
        this.articleStock = articleStock;
    }

    public int getArticleNumber() {
        return articleNumber;
    }

    public String getArticleName() {
        return articleName;
    }

    public double getArticlePrice() {
        return articlePrice;
    }

    public int getArticleStock() {
        return articleStock;
    }


    @Override
    public String toString() {
        return "Article{" +
                "articleNumber=" + articleNumber +
                ", articleName='" + articleName + '\'' +
                ", articlePrice=" + articlePrice +
                ", articleStock=" + articleStock +
                '}';
    }
}
