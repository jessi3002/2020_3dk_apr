import java.net.PortUnreachableException;
import java.sql.*;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        new ArticleManagementGUI();

        ArticleTableModel tableModel = new ArticleTableModel();
        ArticleDataAccessObject articleDAO = new ArticleDataAccessObject("localhost", "3306", "article_management", "root", "");


        ProductInputPanel productInputPanel = new ProductInputPanel(articleDAO, tableModel);
    }

}


