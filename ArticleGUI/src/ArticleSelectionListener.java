import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class ArticleSelectionListener implements ListSelectionListener {

    private JTable table;
    private ProductInputPanel productInputPanel;

    public ArticleSelectionListener(JTable table, ProductInputPanel productInputPanel) {
        this.table = table;
        this.productInputPanel = productInputPanel;
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

        if (e.getValueIsAdjusting()) {
            return;
        }

        ListSelectionModel lsm = (ListSelectionModel) e.getSource();
        int selectedRow = lsm.getMinSelectionIndex();
        System.out.println("Row " + selectedRow + " selected");
        int id = (int)table.getValueAt(selectedRow,0);
        System.out.println("Selected id: " + id);

        Object desc = table.getValueAt(selectedRow, 1);
        System.out.println("Selected description: " + desc);

        productInputPanel.loadArticle(id);

    }
}
