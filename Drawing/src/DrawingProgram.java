import javax.swing.*;

public class DrawingProgram extends JFrame {

    public DrawingProgram(String title){
        super(title);

        Canvas canvas = new Canvas();
        add(canvas);

        setVisible(true);
        setSize(200,200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}
