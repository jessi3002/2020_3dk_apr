import javax.swing.*;
import java.awt.*;

public class Canvas extends JPanel {

    public Canvas() {
        setBackground(Color.BLUE);
        setForeground(Color.MAGENTA);
    }

    @Override
    protected void paintComponent(Graphics g) { //alt+eif.
        super.paintComponent(g);
        g.drawLine(10,10,50,50);

    }
}
