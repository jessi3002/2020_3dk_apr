import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GaesteregistrierungFields extends JPanel {

    private JTextField txtvn = new JTextField();
    private JTextField txtnn = new JTextField();
    private JTextField txtst = new JTextField();
    private JTextField txtor = new JTextField();
    private JTextField txtpl = new JTextField();
    private JTextField txtte = new JTextField();
    private JTextField txtem = new JTextField();
    private JTextField txtti = new JTextField();

    private JButton save = new JButton("Speichern");

    private JButton reset = new JButton("Zurücksetzen");

    public GaesteregistrierungFields() {

        save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GuestRegistrationValidator guestRegistrationValidator = new GuestRegistrationValidator();
                try {
                    guestRegistrationValidator.check(txtvn.getText(), txtnn.getText(), txtte.getText(), txtem.getText(), txtti.getText());
                } catch (GuestDataNotValidExeption guestDataNotValidException) {
                    System.err.println(guestDataNotValidException.getMessage());
                }
            }
        });

        setPreferredSize(new Dimension(550, 255));
        setLayout(null);
        setBorder(new LineBorder(Color.RED, 1));

        JLabel vorname = new JLabel("Vorname:");
        JLabel nachname = new JLabel("Nachname:");
        JLabel strasse = new JLabel("Straße:");
        JLabel plz = new JLabel("PLZ:");
        JLabel ort = new JLabel("Ort:");
        JLabel telnum = new JLabel("Telefonnummer:");
        JLabel email = new JLabel("E-Mail:");
        JLabel tischnr = new JLabel("Tisch-Nr.:");

        vorname.setBounds(20, 0, 100, 25);
        nachname.setBounds(20, 25, 100, 25);
        strasse.setBounds(20, 50, 100, 25);
        plz.setBounds(20, 75, 100, 25);
        ort.setBounds(20, 100, 100, 25);
        telnum.setBounds(300, 0, 100, 25);
        email.setBounds(300, 25, 100, 25);
        tischnr.setBounds(300, 50, 100, 25);

        add(vorname);
        add(nachname);
        add(strasse);
        add(plz);
        add(ort);
        add(telnum);
        add(email);
        add(tischnr);

        txtvn.setBounds(120, 5, 100, 20);
        txtnn.setBounds(120, 27, 100, 20);
        txtst.setBounds(120, 49, 100, 20);
        txtor.setBounds(120, 73, 100, 20);
        txtpl.setBounds(120, 95, 100, 20);
        txtte.setBounds(400, 5, 100, 20);
        txtem.setBounds(400, 27, 100, 20);
        txtti.setBounds(400, 49, 100, 20);

        add(txtvn);
        add(txtnn);
        add(txtst);
        add(txtor);
        add(txtpl);
        add(txtte);
        add(txtem);
        add(txtti);

        save.setBounds(150, 200, 150, 25);
        reset.setBounds(300, 200, 150, 25);

        add(save);
        add(reset);

    }
}
