import javax.swing.*;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane;
import java.awt.*;
import java.awt.event.*;

public class MouseKlickCounterGUI extends JFrame implements MouseListener{

    double counter = 0;
    int number = 0;

    public MouseKlickCounterGUI(String title) {
        super(title);

        setLayout(new FlowLayout());

        JLabel Lnumber = new JLabel();
        Lnumber.setText(Double.toString(number));

        JButton Badd = new JButton("add");
        ActionListener ALadd = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                number += counter;
                counter = 0;
                Lnumber.setText(Integer.toString(number));
            }
        };

        addMouseListener(this);
        this.add(Badd);
        this.add(Lnumber);
        Badd.addActionListener(ALadd);


        setVisible(true);
        setSize(300, 100);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        counter++;
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

}

//Zählt die Klicke und zählt diese zu den bereits geklickten Klicke dazu
