import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class BallCreadingMouseAdapter extends MouseAdapter {

    private PingPongPanel pingPongPanel;
    private final PingPongBar bar;

    public BallCreadingMouseAdapter(PingPongPanel pingPongPanel, PingPongBar bar) {

        this.pingPongPanel = pingPongPanel;
        this.bar = bar;
    }

    @Override
    public void mousePressed(MouseEvent e) {
        PingPongBall ball = new PingPongBall(this.pingPongPanel, e.getX(), e.getY(), this.bar);
        ball.start();
    }

}
