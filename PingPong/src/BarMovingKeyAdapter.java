import javax.swing.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class BarMovingKeyAdapter extends KeyAdapter {

    private final PingPongBar bar;

    private static final int KEY_CODE_RIGHT_ARROW = 39;
    private static final int KEY_CODE_LEFT_ARROW = 37;

    public BarMovingKeyAdapter(PingPongBar bar) {
        this.bar = bar;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int keyCode = e.getKeyCode();
        switch (keyCode) {
            case KeyEvent.VK_LEFT:
                bar.moveLeft();
                break;
            case KeyEvent.VK_RIGHT:
                bar.moveRight();
        }
    }
}
