import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class PingPongPanel extends JPanel {

    public PingPongPanel() {

        setBackground(Color.YELLOW);
        setFocusable(true);

    }

    public void initBar() {
        PingPongBar bar = new PingPongBar(this);
        addKeyListener(new BarMovingKeyAdapter(bar));
        addMouseListener(new BallCreadingMouseAdapter(this, bar));
    }
}
