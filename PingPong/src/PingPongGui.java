import javax.swing.*;
import java.awt.*;

public class PingPongGui extends JFrame {

    public PingPongGui() {
        super("PingPong");

        PingPongPanel canvas = new PingPongPanel();


        setSize(400,400);
        setLocationRelativeTo(null);
        setContentPane(canvas);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);


        canvas.initBar();
    }
}
