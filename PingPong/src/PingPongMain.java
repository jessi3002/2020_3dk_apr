import java.util.Map;

public class PingPongMain {
    public static void main(String[] args) {

        String number = System.getenv("PingPong");

        System.out.println("Read specific environment variable");
        System.out.println("PingPong: " + System.getenv("PingPong"));


        try{
            Integer.parseInt(number);
        }catch (NumberFormatException e){
            System.err.println("Parameter 'number' must have a number");
            System.exit(-1);
        }

        if (Integer.parseInt(number) == 0) {
            System.out.println("Parameter 'number' is missing");
            System.exit(-1);
        }

        new PingPongGui(Integer.parseInt(number));
    }
}

