import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MessageDataAccessObject {

    private DatabaseAccess databaseAccess;

    public MessageDataAccessObject(String host, String port, String database, String user, String password, String dbVendor) {
        try {
            databaseAccess = new DatabaseAccess(host, port, database, user, password, dbVendor);
        } catch (DbVendorNotSupportedException e) {
            System.err.println("Could not start program!");
            System.exit(1);
        }

    }


    public void insert(String text, String language) throws SQLException {
        Connection connection = connect();
        String sql = "insert into message(text, language) VALUES ('" + text + "','" + language + "')";
        //Statement insert = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, text);
            preparedStatement.setString(2, language);
            /*insert = connection.createStatement();
            insert.execute(sql);*/
            System.out.println("Insert executed successfully");
        }catch (SQLException e) {
            throw new RuntimeException("Insert was not executed successfully");
        } finally {
            close(connection);
        }

    }


    public List<Message> readAllMassages() {
        Connection connection = connect();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            String sql = "select * from message";
            ResultSet resultSet = statement.executeQuery(sql);
            ArrayList<Message> messages = new ArrayList<>();
            while (resultSet.next()) {
                int id = resultSet.getInt(1);
                String msg = resultSet.getString(2);
                String language = resultSet.getString(3);
                messages.add(new Message(id, msg, language));
            }
            close(connection);
            return messages;
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not create statement", throwables);
        } finally {
            close(connection);
        }
    }

    private Connection connect() {
        return databaseAccess.connect();
    }

    private void close(Connection connection) {
        databaseAccess.close(connection);
    }


    public void update(Message message) {
        Connection connection = connect();
        System.out.println("updated Execute successfully");
        //Statement statement = null;
        String sql = "update message set text = ?, language = ?, where id = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, message.getLanguage());
            preparedStatement.setString(2, message.getMessage());
            preparedStatement.setInt(3, message.getId());
            /*statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            ResultSet resultSet1 = statement.executeQuery(sql1);*/

            /*while (resultSet.next()) {
                String msg = resultSet.getString(2);
            }*/
        }
        catch (SQLException throwables) {
            throw new RuntimeException("Could not create statement", throwables);
        } finally {
            close(connection);
        }
    }

    public void deleted(int id) {
        Connection connection = connect();
        System.out.println("deleted Execute successfully");
        //Statement statement = null;
        String sql = "delete from message where id = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            /*statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);*/
            }
        catch (SQLException throwables) {
            throw new RuntimeException("Could not create statement", throwables);
        } finally {
            close(connection);
        }
    }


    public List<Message> read(int iD) {
        Connection connection = connect();
        System.out.println("insert Execute successfully");
        Statement statement = null;
        String sql = "select * from message where id = " + iD;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            ArrayList<Message> messages = new ArrayList<>();
            while (resultSet.next()) {
                int id = resultSet.getInt(1);
                String msg = resultSet.getString(2);
                String language = resultSet.getString(3);
                messages.add(new Message(id, msg, language));
            }
            close(connection);
            return messages;
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not create statement", throwables);
        } finally {
            close(connection);
        }
    }

}
