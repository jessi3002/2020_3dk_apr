import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class HelloDatabaseMain {

    public static void main(String[] args) {

        String dbVendor;
        if (args[0].equals("oracle")) {
            dbVendor = "oracle";
        } else {
            dbVendor = "mariadb";
        }

        MessageDataAccessObject messageDAO = new MessageDataAccessObject("Localhost", "3306", "hello_world", "root", "", dbVendor);
        List<Message> messages = messageDAO.readAllMassages();
        messages.forEach(System.out::println);
        /*for (Message msg: messages) {
            System.out.println(msg);
        }*/

        /*try {
            messageDAO.insert("Hello Wordl", "e");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }*/

        List<Message> msg = messageDAO.read(2);
        System.out.println(msg);

        Message message = new Message(5, "Hallooo Welt", "de");
        messageDAO.update(message);

        messageDAO.deleted(12);
    }
}
