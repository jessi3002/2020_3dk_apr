public class MyThread extends Thread{

    private int sleepTime;
    private String name;

    public MyThread(String name, int sleepTime) {
        this.name = name;
        this.sleepTime = sleepTime;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(this.sleepTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Mein Name ist "+ this.name);
    }

}
