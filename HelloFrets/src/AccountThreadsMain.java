public class AccountThreads {
    public static void main(String[] args) {
        Account account = new Account(50);

        Thread alice = new PersonThread("Alice");
        Thread bob = new PersonThread("Bob");
        alice.start();
        bob.start();
    }
}
