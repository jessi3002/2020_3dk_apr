import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class WindowClosingListener extends WindowAdapter {

    @Override
    public void windowClosing(WindowEvent e) {
        System.out.println("Window closing now");
        System.exit(0);
    }

}

// Modul MouseKlickCounter - Kleine GUI mit button und lable - jedes mal wenn man in der Gui klickt soll hochgezählt werden nachdem man den button geklickt hat - BIS FREITAG - ich klicke ins Weiße so oft ich will und klicke dann auf den Button die Zahl im lable wird um die anzahl hochgezählt wie oft ich vorher geklickt hab - MOUSEEVENT; WORDDATEI
