import javax.swing.*;
import java.awt.*;

public class MainTimUndStruppiGUI extends JFrame{
    public MainTimUndStruppiGUI(String title) {
        super(title);

        setLayout(new FlowLayout());

        JButton tim = new JButton("Tim");
        tim.addActionListener(new ConsoleOutputActionListener());

        add(tim);

        JButton and = new JButton("and");
        and.addActionListener(new ConsoleOutputActionListener());

        add(and);

        JButton struppi = new JButton("Struppi");
        struppi.addActionListener(new ConsoleOutputActionListener());
        add(struppi);
        addWindowListener(new WindowClosingListener());

        setVisible(true);
        setSize(300, 100);
    }
}
