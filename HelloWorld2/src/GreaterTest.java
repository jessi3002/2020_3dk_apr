import static org.junit.jupiter.api.Assertions.*;

class GreaterTest {

    @org.junit.jupiter.api.Test
    void greet() {
        Greater greater = new Greater("Tim");
        assertEquals("Hello Tim", greater.greet());
    }

    @Test
    void greetWithNickname() {
        Greater greater = new Greater("Johann");
        assertEquals("Hello Hansi", greater.greet());
    }
}