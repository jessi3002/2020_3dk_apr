import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class ModeSettingActionListener implements ActionListener {

    private final GeometricsFigurs geometricsFigurs;

    public ModeSettingActionListener(GeometricsFigurs geometricsFigurs) {

        this.geometricsFigurs = geometricsFigurs;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println(e.getActionCommand());
        geometricsFigurs.setMode(e.getActionCommand());
    }
}
