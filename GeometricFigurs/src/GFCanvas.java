import javax.swing.*;
import java.awt.*;
public class GFCanvas extends JPanel {

    private int xPos;
    private int yPos;

    private GeometricsFigurs geometricsFigurs;

    public GFCanvas(GeometricsFigurs geometricsFigurs) {
        setBackground(Color.BLACK);
        setPreferredSize(new Dimension(500,550));
        this.geometricsFigurs = geometricsFigurs;

    }
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        setForeground(Color.RED);
        g.drawRect(xPos,yPos,100,100);
        if ("Rechteck".equals(geometricsFigurs.getMode())) {
            g.drawRect(xPos,yPos,100,80);
        } else if ("Kreis".equals(geometricsFigurs.getMode())) {
            g.drawRect(xPos,yPos,100,100);
        } else if ("Scheibe".equals(geometricsFigurs.getMode())) {
            g.drawRect(xPos,yPos,100,100);
        }

    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public void setyPos(int yPos) {
        this.yPos = yPos;
    }


}
