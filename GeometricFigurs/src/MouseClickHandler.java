import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Calendar;

public class MouseClickHandler extends MouseAdapter {

    private GFCanvas gfCanvas;

    public MouseClickHandler(GFCanvas gfCanvas) {
        this.gfCanvas = gfCanvas;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);
        gfCanvas.setxPos(e.getX());
        gfCanvas.setyPos(e.getY());
    }
}
