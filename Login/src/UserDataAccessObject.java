import java.sql.*;

public class UserDataAccessObject {

    private final String connectionurl;
    private final String user;
    private final String password;

    public UserDataAccessObject(String host, String port, String databasename, String user, String password) {
        this.connectionurl = "jdbc:mariadb://" + host + ":" + port + "/" + databasename;
        this.user = user;
        this.password = password;
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        }catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not load database driver", e);
        }
    }

    public boolean login(String user, String password) {
        Connection connection = connect();
        String sql = "SELECT * from login_gui_db.user WHERE username = '" + user + "' and password = '" + password + "'";
        //Statement login = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, user);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            /*login = connection.createStatement();
            ResultSet resultSet = login.executeQuery(sql);*/
            if (resultSet.next()) {
                return true;
            }
            return false;
        }catch (SQLException throwables) {
            throw new RuntimeException("Could not run login query", throwables);
        } finally {
            close(connection);
        }
    }

    public boolean secureLogin(String user, String password) {
        Connection connection = connect();
        String sql = "Select * from user where username = ? and password = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, user);
            preparedStatement.setString(2, password);

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return true;
            }
            return false;
        }catch (SQLException throwables) {
            throw new RuntimeException("Could not run preparedStatement");
        } finally {
            close(connection);
        }
    }

    private Connection connect() {
        try {
            return DriverManager.getConnection(this.connectionurl, this.user, this.password);
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not connect to database", throwables);
        }
    }

    private void close(Connection connection) {
        try {
            connection.close();
        } catch (SQLException throwables) {
            System.err.println("Could not close connection");
        }
    }
}
