import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginGUI extends JFrame {
    public LoginGUI(String title){

        super(title);
        GridLayout layout = new GridLayout(5,2,10,10);
        this.setLayout(layout);

        UserDataAccessObject userDao = new UserDataAccessObject("localhost", "3306", "login_gui_db","root", "");

        JLabel login = new JLabel("Login");
        login.setFont(new Font("San Seriv", Font.PLAIN, 50));
        add(login);

        JPanel leer = new JPanel();
        add(leer);

        JTextField usernameTF = new JTextField();
        usernameTF.setFont(new Font("San Seriv", Font.PLAIN, 25));
        add(usernameTF);

        JLabel username = new JLabel("Benutzername");
        username.setFont(new Font("San Seriv", Font.PLAIN, 25));
        add(username);

        JPasswordField passwordTF = new JPasswordField();
        passwordTF.setFont(new Font("San Seriv", Font.PLAIN, 25));
        add(passwordTF);

        JLabel password = new JLabel("Passwort");
        password.setFont(new Font("San Seriv", Font.PLAIN, 25));
        add(password);

        JButton loginB = new JButton("Login");
        loginB.setFont(new Font("San Seriv", Font.PLAIN, 25));
        add(loginB);
        loginB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("login:        " + userDao.login(usernameTF.getText(), new String(passwordTF.getPassword())));
                System.out.println("secure-login: " + userDao.secureLogin(usernameTF.getText(), new String(passwordTF.getPassword())));
            }
        });

        JButton register = new JButton("Registrieren");
        register.setFont(new Font("San Seriv", Font.PLAIN, 25));
        add(register);

        JButton forgot = new JButton("PW vergessen");
        forgot.setFont(new Font("San Seriv", Font.PLAIN, 25));
        add(forgot);


        setVisible(true);
        setSize(700,550);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
}
